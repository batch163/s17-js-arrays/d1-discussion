console.log("JS ARRAYS");

/*
	Array
		- collection of multiple related data/values
*/

let month = ["Jan", "Feb", "Mar", "Apr", "May", "June"];

// console.log(month);			//array
// console.log(month.length);	//5
// console.log(month[3]);		//Apr

//using for loop
/*
for(let i = 0; i < month.length; i++){

	console.log(`Month of ${month[i]}`);	//displaying each element in an array
}

*/
//How to initialize an array?
	let fruits = ["apple", "banana", "strawberry"];

	let phones = new Array("iphone", "samsung", "nokia");
	// console.log(phones)


/*
	Array Manipulation
*/

	let count = ["one", "two", "three", "four", "five"];
	console.log(count);

	//Push() method
		//adds an element at the end of an array
		count.push("six", "seven");
		console.log(count);

		//using a funtion:
		function Push(element){
			//add "seven" element in an array
			count.push(element)
		}

		Push("seven");
		console.log(count);

		Push("eight");
		console.log(count);

	//Pop() method
		//remove an element at the end  of an array
		count.pop();
		console.log(count)

		//using a function:
		function Pop(){
			count.pop()
		}

		Pop();
		console.log(count);

		Pop();
		console.log(count);


	//Shift() method
		//removes an element at the beginning of an array
		count.shift();
		console.log(count);


	//Unshift() method
		//add an element at the beginning of an array
		count.unshift("one");
		console.log(count);


	let digits = [5, "one", 8, 6, 3, 9, 4, 500, 840, 239, "ten"];

	//Sort() method
		digits.sort();
		console.log(digits);


		digits.sort( 
			function(a, b){
				//ascending order
				// return a - b

				//descending order
				return b - a
			}
		);

		console.log(digits);


/*
	Slice and Splice
*/

	//Splice
		//modifies the original array and returns a new array containing the deleted elements

		//1st param - index number
		//2nd param - # of elements to be deleted
		//3rd param - added in placement of deleted elements

		console.log(month);

	// let nSplice = month.splice(1, 2);
	// let nSplice = month.splice(1, 2, "July", "Aug", "Sept");
	// console.log(month);

	// console.log(nSplice);


	// Slice
		//returns a shallow copy of a portion of an array

		//1st param - index number (start)
		//2nd param - index number where to stop

		console.log(month);

	// let nSlice = month.slice(2);		//from index 2-last element
	// let nSlice = month.slice(2, 4);		//from index 2-3

//Q: if you want to copy the elements from index 3 up to the last element, what values of params should we use?
	
	// let nSlice = month.slice(3, month.length);
	// let nSlice = month.slice(-4);

	// console.log(month);
	// console.log(nSlice);



	//Concat() method
		//to merge two or more arrays
		// console.log(month);
		// console.log(fruits);
		// console.log(phones);

	// let concatArray = month.concat(fruits, phones);
	// console.log(month);
	// console.log(fruits);
	// console.log(phones);

	// console.log(concatArray);


	//Join() method
		// 

		//params:
			//" "
			//"-"
			// ", "
	console.log(month);

	let newJoin = month.join(", ");
	console.log(newJoin);
	// console.log(typeof newJoin);


	//toString() method

	let elements = [1, 2, "a", "3b"];

	let nToString = elements.toString()
	console.log(nToString);
	console.log(typeof nToString);

	
/*
	Accessors
*/	
	let countries = ["US", "RU", "CAN", "PH", "SG", "HK", "PH"];

	//indexOf()
		//we will find PH in the countris array
		//param - value

	console.log(countries.indexOf("PH"));	//3
	console.log(countries.indexOf("NZ"));	//-1
/*
	if(countries.indexOf("NZ") !== -1){
		alert("PH is in the countries array");
	} else {
		alert("Country does not exist");
	}
*/

	//lastIndexOf()
		//param
	console.log(countries.lastIndexOf("PH"));	//6


/*
	Iterators

		//forEach()
			// executes a provided function "once for each element"
			// does not return a value from the function
	
		syntax of forEach():

			referenceArray.forEach(callback fn)


			syntax of callback function:

				function(<parameter>){
					//statement
				}
*/

		console.log(month);

		month.forEach(
			function(element){
				console.log(element)
			}
		);

/*

		//map()
			// returns a new array, w/ different values depending on the result of the function's operation

		syntax:
			refArray.map( 
				function(param){
					//codeblock
				} 
			)

*/
	
let names = ["Angelito", "Ian", "Gerard", "Mikhaella", "Matthew"];

let mapNames = names.map(
		function(student){
			// console.log(student)

			return(
				`
					<p>My name is ${student}</p>
				`
			)
		}
	).join(" ")

let container = document.getElementById('display');

container.innerHTML = mapNames;

/* forEach returns undefined value

let forEachNames = names.forEach(
		function(student){
			// console.log(student)

			return student
		}
	)

console.log(forEachNames)

*/
/*
		//filter()
			//creates a new array that contains elements which passed a given condition

			//does not modify the original array

		syntax: 

		refArray.filter(function(){
			//statement
		})


*/
		let nums = [1, 2, 3, 4, 5];

		let filteredNums = nums.filter(function(red){
			// console.log(red)

			return red < 4
		})

		console.log(filteredNums);


/*

		//includes
			//determines whether an array includes a certain value among its entries, returning true or false value

*/
		let animals = ["dog", "cat", "bird", "fish"];

		console.log( animals.includes("dinasaur") )

	/*
		Mini activity

			Using a function, return the word if the word exists in the array, else return false or animal not existing

	*/

		function checkWord(word){

			if(animals.includes(word)){
				return word
			} else {
				return `Animal does not exist`
			}
		}

		console.log(checkWord("bird"));
		console.log(checkWord("horse"));

/*
		//Every()
			// The every() method tests whether all elements in the array pass the test implemented by the provided function. It returns a Boolean value.
*/
	console.log(nums)

	let result = nums.every(function(element){
		return element > 1
	})

	console.log(result)

/*
		//Some()
			// The every() method tests whether all elements in the array pass the test implemented by the provided function. It returns a Boolean value.
*/	
	let someResult = nums.some(function(element){
		return element > 5
	})

	console.log(someResult)